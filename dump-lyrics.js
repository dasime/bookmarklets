/* Dumps Lyrics from http://www.kasi-time.com and https://www.uta-net.com to the console.
 *
 * For example: http://www.kasi-time.com/item-45531.html
 * For example: https://www.uta-net.com/song/241342/
 */

// Normal

(function () {
  switch (window.location.host) {
    case 'www.kasi-time.com':
      console.log(document.getElementById("lyrics").innerText);
      break;
    case 'www.uta-net.com':
      console.log(document.getElementById("kashi_area").innerText);
      break;
    default:
      console.log('unsupported%20site');
  }
})()

// Minified

javascript:(function(){switch(window.location.host){case"www.kasi-time.com":console.log(document.getElementById("lyrics").innerText);break;case"www.uta-net.com":console.log(document.getElementById("kashi_area").innerText);break;default:console.log("unsupported%20site")}})()
