/*
 * Set the video playback speed of the first video element on the page.
 */

// Video Increased by 10%
javascript:(function(){document.getElementsByTagName('video')[0].playbackRate+=0.1;})();

// Video Decreased by 10%
javascript:(function(){document.getElementsByTagName('video')[0].playbackRate-=0.1;})();

// Video 100%
javascript:(function(){document.getElementsByTagName('video')[0].playbackRate=1;})();

// Video 125%
javascript:(function(){document.getElementsByTagName('video')[0].playbackRate=1+(1/4)})();

// Video 133%
javascript:(function(){document.getElementsByTagName('video')[0].playbackRate=1+(1/3);})();

// Video 150%
javascript:(function(){document.getElementsByTagName('video')[0].playbackRate=1+(1/2);})();
