/*
 * Twitter will by collapse sensitive images by default when not logged in.
 *
 * This will attempt to click "View" on any loaded tweets that have that a
 *   button that says "View".
 *
 * NOTE: Twitter history unloads as you scroll around.
 *
 * NOTE: Using a loop in this bookmarklet is disallowed by Twitter's CSP policy.
 *
 * Designed for: https://twitter.com/
 *
 */

// Normal

(function(){
  [
    ...document.getElementsByTagName('article')
  ].forEach(tweet => {
    tweet.querySelectorAll('[role="button"]').forEach(button => {
      button.innerText === "View" ? button.click() : null
    })
  });
})()

// Minified

javascript:(function(){[...document.getElementsByTagName('article')].forEach(tweet=>{tweet.querySelectorAll('[role="button"]').forEach(button=>{button.innerText==="View"?button.click():null})});})()
