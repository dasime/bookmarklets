/*
 * Print the page title to an alert box. HTML encode the pipe characters.
 *
 * This is useful for citing webpages for Wikipedia.
 */

javascript:alert(document.title.replace('|','&#124;'))
