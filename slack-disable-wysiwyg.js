/*
 * Slack introduced a new WYSIWYG editor under a feature flag,
 *   but didn't add an official way to disable it.
 * The problem is, it's actually incomplete - and breaks when you paste
 *   blobs of mrkdwn (Slack's own half-compatible version of Markdown).
 *
 * Designed for: https://slack.com/
 *
 * Author:  Kevin Fahy (https://github.com/kfahy)
 * Repo:    https://github.com/kfahy/slack-disable-wysiwyg-bookmarklet
 * License: MIT
 *
 */

javascript:(function() {
  const teamId = window.slackDebug.activeTeamId;
  const redux = window.slackDebug[teamId].redux;
  const experiments = redux.getState().experiments;
  const filteredExperiments = {};
  for (const key in experiments) {
    if (!key.includes('wysiwyg')) {
      filteredExperiments[key] = experiments[key];
    }
  }
  redux.dispatch({
    type: '[19] Bulk add experiment assignments to redux',
    payload: filteredExperiments
  });
})();
