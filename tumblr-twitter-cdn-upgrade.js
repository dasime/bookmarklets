/*
 * Upgrade images from Tumblr and Twitter to "high res".
 */

// Normal

(function () {
  if (window.location.host.includes('media.tumblr.com')) {
    window.location = window.location.href.replace(/_\d\d\d.(png|jpg|gif)/, '_1280.$1');
  } else if (window.location.host.includes('pbs.twimg.com')) {
    if (window.location.pathname.search(/:orig$/) > -1) {
      window.alert('twittier%20image%20already%20upgraded');
    } else if (window.location.pathname.search(/:large$/) > -1) {
      window.location = window.location.href.replace(':large', ':orig');
    } else {
      window.location = window.location.href.concat(':orig');
    }
  } else {
    window.alert('not%20a%20tumblr%20or%20a%20twitter%20image');
  };
})()

// Minified

javascript:(function(){if(window.location.host.includes('media.tumblr.com')){window.location=window.location.href.replace(/_\d\d\d.(png|jpg|gif)/,'_1280.$1')}else if(window.location.host.includes('pbs.twimg.com')){if(window.location.pathname.search(/:orig$/)>-1){window.alert('twittier%20image%20already%20upgraded')}else if(window.location.pathname.search(/:large$/)>-1){window.location=window.location.href.replace(':large',':orig')}else{window.location=window.location.href.concat(':orig')}}else{window.alert('not%20a%20tumblr%20or%20a%20twitter%20image')}})();
