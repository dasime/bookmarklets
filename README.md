# Bookmarklets

## bookmarklets-all.html

**Author:** Jesse Ruderman (<https://www.squarefree.com/>)  
**Source:** <https://www.squarefree.com/bookmarklets/>

I've been using these bookmarklets for years, the ones I use regularly are these "zap" ones:

* `remove redirects` - _"Changes redirecting links to go directly to the "real" target."_
* `restore context menu` - _"Fixes pages that disable context menus."_
* `restore selecting` - _"Fixes pages that disable text selection."_
* `zap timers` - _"Removes timers that were created with setTimeout or setInterval."_

For more information about the purpose of these bookmarklets check out the above source.

## dump-kasi-time.js

**Author:** Original

Dumps lyrics from <http://www.kasi-time.com/> to the console.
