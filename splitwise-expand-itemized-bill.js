/*
 * Expand the "itemized list" [sic] input field on Splitwise so it's long
 *   enough to read and input large names.
 *
 * Designed for: https://secure.splitwise.com/
 *
 */

javascript:(function(){document.getElementsByTagName('style')[0].sheet.insertRule('input.item_name{width:500px;}',0);})()
