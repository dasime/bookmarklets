/*
 * Steam Multibuy Card Sets
 * 
 * If you're buying cards to complete a set on Steam, there is often a useful
 *    "Buy remaining cards on the Market" button to help with the checkout.
 * 
 * This button only shows up when the "Craft Badge" button is not available.
 * 
 * Additionally, this button only attempts to craft one level,
 *    even though you'd usually want enough cards to hit level 5.
 * 
 * This bookmarklet attempts to fix both of these issues:
 * 
 * 1. Allows you to use the multibuy checkout even when "Craft Badge" is shown.
 * 2. Will read the current badge level, amount of each card you have,
 *    and automatically set the desired quantity to the amount required
 *    to reach level 5.
 *
 * Designed for: https://steamcommunity.com/id/${USER}/gamecards/${GAME_GUID}/
 * 
 * Good URL to test it on:
 *    https://steamcommunity.com/id/dasime/gamecards/246900/
 * Has a mix of cards with the " (Trading Card)" suffix, and cards with an "&".
 */

// Full fat version

(function(){
  /** Guards **/

  // Game GUID
  const gameGuid = parseInt(window.location.pathname.match(/\d+/)[0]);
  const foilCard = window.location.search === '?border=1';

  // Target level
  // Normal standard badges stop at level 5
  // Normal foil badges stop at level 1
  const TARGET_LEVEL = foilCard ? 1 : 5;

  // Current Level
  const detectLevel = document.getElementsByClassName("badge_info")[0]
                              .innerText
                              .match(/Level (\d)/);
  const currentLevel = detectLevel ? parseInt(detectLevel[1]) : 0;

  const badgeProgress = document.getElementsByClassName('gamecard_badge_progress');

  // Badge progress is not available, this is usually for non-card badges.
  // Example: (Years of Service     - time)
  //          https://steamcommunity.com/id/dasime/badges/1/
  // Example: (Community Ambassador - tasks)
  //          https://steamcommunity.com/id/dasime/badges/2/
  // Example: (Lunar New Year 2019  - points)
  //          https://steamcommunity.com/id/dasime/badges/34
  if (badgeProgress.length === 0) {
    alert("This badge is not levelled with cards.");
    return;
  }

  // Badge is already max level.
  // Example: (GitS - 5/5)
  //          https://steamcommunity.com/id/dasime/gamecards/369200/
  if (badgeProgress[0].innerText === "You have leveled this badge to completion.") {
    alert("You have levelled this badge to completion.");
    return;
  }

  // Badge can no longer be levelled
  // Seen on Steam Holiday card sets.
  // Example: (Holiday Sale 2013 Badge)
  //          https://steamcommunity.com/id/dasime/gamecards/267420/
  if (currentLevel > 0 && badgeProgress[0].innerText === "") {
    alert("This badge can no longer be levelled.");
    return;
  }

  let remainingLevels;

  if (currentLevel >= TARGET_LEVEL) {
    // Special badges are often unlimited, so assume the minimum (1 level)
    // Example: (Steam Summer Getaway)
    //          https://steamcommunity.com/id/dasime/gamecards/245070/
    //          This is an unusual example of an event card that doesn't expire
    remainingLevels = 1;
  } else {
    remainingLevels = TARGET_LEVEL - currentLevel;
  }

  const baseCardSuffix = foilCard ? " (Foil)" : "";
  const specialCardSuffix = foilCard ? " (Foil Trading Card)" : " (Trading Card)";

  /** Logic **/

  // Some card names for some reason have " (Trading Card)" suffix on their name
  // To detect this we POST the marketplace to see if the card name works
  //   without this suffix, if it returns 500, we append it.
  const confirmTradingCardSuffix = (cardName, offset) => {
    const pricingQueryURI = new URL('https://steamcommunity.com/market/priceoverview/?appid=753');

    pricingQueryURI.search += `&market_hash_name=${gameGuid}-${encodeURIComponent(cardName)}`;

    // Using synchronous XMLHttpRequest() rather than fetch() because
    //   Array.filter is synchronous and this code is lazy
    const xhr = new XMLHttpRequest();
    xhr.open(
      'POST',
      pricingQueryURI.href,
      false  // synchronous
    );

    // This API is rate limited, to avoid network issues,
    //    increase backoff for each subsequent card.
    setTimeout(xhr.send(), offset);

    return xhr.status === 500 ?
      `${cardName}${specialCardSuffix}` :
      `${cardName}${baseCardSuffix}`;
  };

  // Find the cards in the current set and how many currently owned
  const setList = [
    ...document.getElementsByClassName("badge_card_set_cards")[0]
               .getElementsByClassName("badge_card_set_text"),
  ]
    // Remove "x of y, Series z" information (which item it is in the set)
    // Sets can have more than 9 cards
    .filter(element => !/\d+ of \d+, Series \d/.test(element.innerText))
    // Remove nobody to trade with information
    .filter(element => {
      return element.innerText !== "None of your friends have this card.";
    })
    // Split the current count and name
    .map(element => {
      const cardStatus = element.innerText
        .replace('\n','')
        .match(/^\(?(\d)?\)?(.+)$/);
      return {
        name: cardStatus[2],
        count: parseInt(cardStatus[1]) || 0,
      };
    })
    // Work out if the card needs " (Trading Card)" appended
    .map((cardStatus, idx) => {
      cardStatus.name = confirmTradingCardSuffix(cardStatus.name, idx * 100);
      return cardStatus;
    });

  // Build multibuy URI
  const multibuyURI = new URL('https://steamcommunity.com/market/multibuy?appid=753');

  setList.forEach(cardStatus => {
    // Don't attempt to buy a negative amount of cards
    const copiesRequired = (remainingLevels - cardStatus.count) >= 0 ?
      remainingLevels - cardStatus.count :
      0;

    multibuyURI.search += `&items[]=${gameGuid}-${encodeURIComponent(cardStatus.name)}`;
    multibuyURI.search += `&qty[]=${copiesRequired}`;
  });

  // Open link in new tab
  window.open(multibuyURI.href, "_blank");
})()

// Minified

// eslint-disable-next-line
(function(){const a=parseInt(window.location.pathname.match(/\d+/)[0]),b="?border=1"===window.location.search,c=b?1:5,d=document.getElementsByClassName("badge_info")[0].innerText.match(/Level (\d)/),e=d?parseInt(d[1]):0,f=document.getElementsByClassName("gamecard_badge_progress");if(0===f.length)return void alert("This badge is not levelled with cards.");if("You have leveled this badge to completion."===f[0].innerText)return void alert("You have levelled this badge to completion.");if(0<e&&""===f[0].innerText)return void alert("This badge can no longer be levelled.");let g=e>=c?1:c-e;const h=b?" (Foil)":"",i=b?" (Foil Trading Card)":" (Trading Card)",j=(b,c)=>{const d=new URL("https://steamcommunity.com/market/priceoverview/?appid=753");d.search+=`&market_hash_name=${a}-${encodeURIComponent(b)}`;const e=new XMLHttpRequest;return e.open("POST",d.href,!1),setTimeout(e.send(),c),500===e.status?`${b}${i}`:`${b}${h}`},k=[...document.getElementsByClassName("badge_card_set_cards")[0].getElementsByClassName("badge_card_set_text")].filter(a=>!/\d+ of \d+, Series \d/.test(a.innerText)).filter(a=>"None of your friends have this card."!==a.innerText).map(a=>{const b=a.innerText.replace("\n","").match(/^\(?(\d)?\)?(.+)$/);return{name:b[2],count:parseInt(b[1])||0}}).map((a,b)=>(a.name=j(a.name,100*b),a)),l=new URL("https://steamcommunity.com/market/multibuy?appid=753");k.forEach(b=>{const c=0<=g-b.count?g-b.count:0;l.search+=`&items[]=${a}-${encodeURIComponent(b.name)}`,l.search+=`&qty[]=${c}`}),window.open(l.href,"_blank")})();
